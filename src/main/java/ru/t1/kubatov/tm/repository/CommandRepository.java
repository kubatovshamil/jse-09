package ru.t1.kubatov.tm.repository;

import ru.t1.kubatov.tm.api.repository.ICommandRepository;
import ru.t1.kubatov.tm.model.Command;
import ru.t1.kubatov.tm.constant.CommandConstant;
import ru.t1.kubatov.tm.constant.ArgumentConstant;

public final class CommandRepository implements ICommandRepository {

    private static final Command VERSION = new Command(
            CommandConstant.VERSION, ArgumentConstant.VERSION, "Show application version."
    );

    private static final Command HELP = new Command(
            CommandConstant.HELP, ArgumentConstant.HELP, "Show application commands."
    );

    private static final Command INFO = new Command(
            CommandConstant.INFO, ArgumentConstant.INFO, "Show developer info."
    );

    private static final Command COMMANDS = new Command(
            CommandConstant.COMMANDS, ArgumentConstant.COMMANDS, "Show application commands."
    );

    private static final Command ARGUMENTS = new Command(
            CommandConstant.ARGUMENTS, ArgumentConstant.ARGUMENTS, "Show application arguments."
    );

    private static final Command EXIT = new Command(
            CommandConstant.EXIT, null, "Close application."
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConstant.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConstant.PROJECT_LIST, null,
            "Show project list."
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConstant.PROJECT_CLEAR, null,
            "Delete all projects."
    );

    private static final Command PROJECT_SHOW_BY_ID = new Command(
            CommandConstant.PROJECT_SHOW_BY_ID, null, "Show Project by ID."
    );

    private static final Command PROJECT_SHOW_BY_INDEX = new Command(
            CommandConstant.PROJECT_SHOW_BY_INDEX, null, "Show Project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            CommandConstant.PROJECT_UPDATE_BY_ID, null, "Update Project by ID."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            CommandConstant.PROJECT_UPDATE_BY_INDEX, null, "Update Project by index."
    );

    private static final Command PROJECT_DELETE_BY_ID = new Command(
            CommandConstant.PROJECT_DELETE_BY_ID, null, "Delete Project by ID."
    );

    private static final Command PROJECT_DELETE_BY_INDEX = new Command(
            CommandConstant.PROJECT_DELETE_BY_INDEX, null, "Delete Project by index."
    );

    private static final Command TASK_CREATE = new Command(
            CommandConstant.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command TASK_LIST = new Command(
            CommandConstant.TASK_LIST, null,
            "Show task list."
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConstant.TASK_CLEAR, null,
            "Delete all tasks."
    );

    private static final Command TASK_SHOW_BY_ID = new Command(
            CommandConstant.TASK_SHOW_BY_ID, null, "Show Task by ID."
    );

    private static final Command TASK_SHOW_BY_INDEX = new Command(
            CommandConstant.TASK_SHOW_BY_INDEX, null, "Show Task by index."
    );

    private static final Command TASK_SHOW_BY_PROJECT_ID = new Command(
            CommandConstant.TASK_SHOW_BY_PROJECT_ID, null, "Show Task by Project ID."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            CommandConstant.TASK_UPDATE_BY_ID, null, "Update Task by ID."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            CommandConstant.TASK_UPDATE_BY_INDEX, null, "Update Task by index."
    );

    private static final Command TASK_DELETE_BY_ID = new Command(
            CommandConstant.TASK_DELETE_BY_ID, null, "Delete Task by ID."
    );

    private static final Command TASK_DELETE_BY_INDEX = new Command(
            CommandConstant.TASK_DELETE_BY_INDEX, null, "Delete Task by index."
    );

    private static final Command TASK_BIND_TO_PROJECT = new Command(
            CommandConstant.TASK_BIND_TO_PROJECT, null, "Bind Task to Project."
    );

    private static final Command TASK_UNBIND_FROM_PROJECT = new Command(
            CommandConstant.TASK_UNBIND_FROM_PROJECT, null, "Unbind Task from Project."
    );

    private static final Command[] COMMANDS_ARRAY = new Command[]{
            VERSION, HELP, INFO, COMMANDS, ARGUMENTS,

            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_DELETE_BY_ID, PROJECT_DELETE_BY_INDEX,

            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_SHOW_BY_PROJECT_ID,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_DELETE_BY_ID, TASK_DELETE_BY_INDEX,
            TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT,

            EXIT

    };

    @Override
    public Command[] getTerminalCommands() {
        return COMMANDS_ARRAY;
    }

}
